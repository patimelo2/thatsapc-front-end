# -*- coding: utf-8 -*-

import os
from subprocess import PIPE, Popen

# Detect os for choosing api name:
if os.name == "nt":
    APIPath = "binaries/thatsapc-api.exe"
else:
    APIPath = "binaries/thatsapc-api"

class Chat:
    # Singleton pattern:
    __instance = None
    def __init__(self):
        if Chat.__instance is None:
            Chat.__instance = Chat.__impl()

        self.__dict__['_Chat__instance'] = Chat.__instance
    def __getattr__(self, attr):
        return getattr(self.__instance, attr)

    def __setattr__(self, attr, value):
        return setattr(self.__instance, attr, value)
    
    class __impl:
        def __init__(self):
            self.__proc = Popen([APIPath], stdout=PIPE, stdin=PIPE)
            self.__opened = True
            
        # Write methods:
        def _WriteString(self,string):
            if (not self.__opened):
                return
            self.__proc.stdin.write(string+"\n")
        
        # Read methods:
        def _readBoolean(self):
            return self._readString() == "true"

        def _readString(self):
            if (not self.__opened):
                return ""
            return self.__proc.stdout.readline().strip(" \n\r\t")
        
        def _readInteger(self):
            if (not self.__opened):
                return 0
            return int(self._readString())
        
        def _readChunk(self):
            if (not self.__opened):
                return ""                
            chunk = self.__proc.stdout.read(self._readInteger())
            # Ignore newline:
            self._readString()
            return chunk
        
        def _readMultiString(self):
            lines = self._readInteger()
            result = ""
            for i in range(lines):
                result += self._readString() + '\n'
            return result[:-1]
        
        # Debug node:
        def printnode(self):
            self._WriteString("printnode")
            print self._readMultiString()
        
        # API methods:
        def connect(self):
            self._WriteString("connect")
            return self._readBoolean()
        
        def connected(self):
            self._WriteString("connected")
            return self._readBoolean()
        
        def login(self,phone,password,nick):
            self._WriteString("login " + phone + " " + password + " " + nick)
            return self._readBoolean()
        
        def relogin(self):
            self._WriteString("relogin")
            return self._readBoolean()
        
        def logged(self):
            self._WriteString("logged")
            return self._readBoolean()
        
        def sendmessage(self, phoneID, txt, msgid = "AUTO"):
            self._WriteString("sendmessage "+msgid+" "+phoneID+" "+str(txt.count("\n")+1))
            self._WriteString(txt)            
        
        def pop(self):
            self._WriteString("pop")
            return self._readBoolean()
        
        def gettag(self):
            self._WriteString("gettag")
            return self._readString()
        
        def getattribute(self,attr):
            self._WriteString("getattribute "+attr)
            return self._readString()
        
        def getvalue(self):
            self._WriteString("getvalue")
            return self._readChunk()
        
        def gochild(self,child):
            self._WriteString("gochild "+child)
            return self._readBoolean()
        
        def goparent(self):
            self._WriteString("goparent")
            return self._readBoolean()
        
        def close(self):
            self.__opened = False
            self._WriteString("disconnect")
            self.__proc.kill()
            
        
           
        
