from UiDialog import Ui_Dialog
from PyQt4 import QtCore, QtGui

class Dialog(QtGui.QDialog):
	def __init__(self, text, wtext = "Dialog", btext = "Ok"):
		QtGui.QDialog.__init__(self);

		self.ui = Ui_Dialog();
		self.ui.setupUi(self);
		self.setWindowTitle(wtext);
		self.ui.label.setText(text);
		self.ui.pushButton.setText(btext);

		self.connect(self.ui.pushButton, QtCore.SIGNAL('clicked()'), QtCore.SLOT('close()'));
		self.show();
